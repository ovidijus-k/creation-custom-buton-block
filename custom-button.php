<?php
/*
Plugin Name: Custom button block
Plugin URI:
Description: Custom button block
Version: 1.0
Author: Creation
Author URI:
Update URI:
*/

/**
 *
 * @return void
 */
function creationRegisterCustomButtonBlock()
{
    wp_enqueue_script(
        'creation-custom-button',
        plugin_dir_url(__FILE__) . 'block.js',
        ['wp-blocks', 'wp-i18n', 'wp-editor'],
        true
    );
}

add_action('enqueue_block_editor_assets', 'creationRegisterCustomButtonBlock');
