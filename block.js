wp.blocks.registerBlockType( 'creation/custom-button', {
    apiVersion: 2,

    // Built-in attributes

    title: 'Custom button',                 // Title
    description: 'Output custom button',    // Description
    icon: 'megaphone',                      // Dashboard icon or inline svg
    category: 'widgets',                    // Category where block can be found in block editor

    // Custom attributes

    attributes: {
        label: {
            type: 'string',
        },
        url: {
            type: 'string',
        },
        bgcolor: {
            type: 'string',
        },
        color: {
            type: 'string',
        },
    },

    // Built-in function

    // Edit interface in dashboard
    edit: function ( props ) {
        function updateLabel(event) {
            props.setAttributes({label: event.target.value});
        }

        function updateUrl(event) {
            props.setAttributes({url: event.target.value});
        }

        function updateColor(value) {
            props.setAttributes({color: value.hex});
        }

        function updateBgColor(value) {
            props.setAttributes({bgcolor: value.hex});
        }

        return wp.element.createElement(
            "div",
            null,
            wp.element.createElement("h4", null, "Label"),
            wp.element.createElement("input", {
                type: "text",
                value: props.attributes.label,
                onChange: updateLabel
            }),
            wp.element.createElement("h4", null, "URL"),
            wp.element.createElement("input", {
                type: "text",
                value: props.attributes.url,
                onChange: updateUrl
            }),
            wp.element.createElement("h4", null, "Text color"),
            wp.element.createElement(wp.components.ColorPicker, {
                color: props.attributes.color,
                onChangeComplete: updateColor
            }),
            wp.element.createElement("h4", null, "Background color"),
            wp.element.createElement(wp.components.ColorPicker, {
                color: props.attributes.bgcolor,
                onChangeComplete: updateBgColor
            })
        );
    },
    // Output HTML on website frontend
    save: function( props ) {
        return wp.element.createElement(
            "a",
            {
                class: 'btn',
                style: {
                    background: props.attributes.bgcolor,
                    color: props.attributes.color
                },
                href: props.attributes.url
            },
            props.attributes.label
        );
    }
});
